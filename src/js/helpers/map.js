/**
 * Api Map
 */
window.App = window.App || {};

//construtor da classe
window.App.Map = function(){
	'use strict';

	var map,
		marker,
		directionsService,
		directionsDisplay,
		apiPublica = {
			//será chamada quando a pagina estiver pronta
			onPageReady: function(latitude, longitude, label){
				directionsService = new google.maps.DirectionsService();
				directionsDisplay = new google.maps.DirectionsRenderer();
				var latlng = new google.maps.LatLng(latitude, longitude),
					options = {
						zoom: 16,
						center: latlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						mapTypeControl: false, scrollwheel: false
					};

				map = new google.maps.Map(document.getElementById('map'), options);
				directionsDisplay.setMap(map);

				marker = new google.maps.Marker({
					position: latlng,
					map: map,
					title: label,
					icon: 'assets/images/marker.png'
				});
				marker.addListener('click', toggleBounce);
			}
		},
		toggleBounce = function(){
			if (marker.getAnimation() !== null) {
				marker.setAnimation(null);
			} else {
				marker.setAnimation(google.maps.Animation.BOUNCE);
			}
		};

	return apiPublica;
};