jQuery(document).ready(function($) {

    // Cache selectors
var topMenu =$(".navbar-fixed-top"),
topMenuHeight = topMenu.outerHeight()+15,
// All list items
menuItems = topMenu.find("a"),
// Anchors corresponding to menu items
scrollItems = menuItems.map(function(){
  var item = $($(this).attr("href"));
  if (item.length) { return item; }
});
menuItems.hide();

// Bind to scroll
$(window).scroll(function(){
    // Get container scroll position
    var fromTop = $(this).scrollTop()+topMenuHeight;

    // Get id of current scroll item
    var cur = scrollItems.map(function(){
        if ($(this).offset().top < fromTop)
        return this;
    });
    
    // Get the id of the current element
    cur = cur[cur.length-1];
    
    var id = cur && cur.length ? cur[0].id : "";
    
    // Set/remove active class
    menuItems.removeClass("active");
    
    $(".navbar-fixed-top a[href='#"+id+"']").addClass("active");

    var $nav = $(".navbar-fixed-top");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
});
    

    

    $('a[href^="#"]').on('click', function(e) {
        e.preventDefault();
        var id = $(this).attr('href'),
                targetOffset = $(id).offset().top;
                
        $('html, body').animate({ 
            scrollTop: targetOffset 
        }, 500);
    });
    

    setTimeout(function(){ 
        menuItems.removeClass("active").fadeIn();
    },250)
    
    if($('form.validate-alta').length > 0){


        $('form.validate-alta').each(function(){

            var form = $(this),
                inputsContactForm = [],
                emailadress = '';

            //grava as propriedades de cada input
            form.find('input, textarea, select').each(function(){
                //console.log('item')

                if($(this).hasClass('validate')){
                    var name = $(this).prop('name'),
                        type = $(this).prop('type'),
                        mask;

                    if($(this).attr('data-mask') === undefined)
                        mask = false;
                    else{
                        mask = $(this).attr('data-mask');
                        $(this).addClass(mask);
                    }

                    inputsContactForm.push([name, type, mask]);
                }
            });

            //chama metodo de validação
            validate(form, inputsContactForm);

            /*            if(form.hasClass('send-validation')){
                            $("#feedback").hide();
                            var toast = form.hasClass('send-toast');
                            var trial = form.hasClass('trial');


                            //chama metodo de envio
                            sendAction(
                                form,
                                form.data('msgsuccess'),
                                form.data('msgerror'),
                                '#feedback',
                                false,
                                toast,
                                trial
                            );
            }*/
        });
    }
});


function sendAction(form, msg, msgError, feedbackLabel, backToTop, toast, trial){
    var optionsForm = {
        type: "POST",
        url: form.attr('action'),
        clearForm: true,

        success: function(data){
            $("#feedback").show();

        }
    };

    form.ajaxForm(optionsForm);
}

function phoneMask(input){
    //console.log('phone');
    // using jQuery Mask Plugin v1.7.5
    // http://jsfiddle.net/d29m6enx/2/
    var maskBehavior = function(val){
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        options = {
            onKeyPress: function(val, e, field, options){
                field.mask(maskBehavior.apply({}, arguments), options);
            }
        };

    $('.'+input).mask(maskBehavior, options);
}
function mask(input){

    if(input == 'cnpj')
        $('.'+input).mask('99.999.999/9999-99');
    if(input == 'cpf')
        $('.'+input).mask('999.999.999-99');
    if(input == 'cep')
        $('.'+input).mask('99999-999');
}

function validate(form, campos, toast){
    //console.log('validate');
    var key,
        formRules = {},
        formMsg = {};

    for(key in campos){
        var validRules,
            validMsg;

        //verifica se é um campo tipo email
        if(campos[key][1] == "email"){
            validRules = {
                required: true,
                email: true
            };
            validMsg = {
                required: form.data('required'),
                email: form.data('msgismail')
            };
        }else{
            validRules = {
                required: true
            };
            validMsg = form.data('required');
        }

        //adiciona mascara
        if(campos[key][2] == "phone")
            phoneMask(campos[key][2]);
        else
            mask(campos[key][2]);

        formRules[campos[key][0]] = validRules;
        formMsg[campos[key][0]] = validMsg;
    }

    //implementa o metodo validate
    if(!form.hasClass('no-validate-fields'))
        form.validate({rules: formRules, messages: formMsg});
}