<h2 align="center">Bilbo Boilerplate</h2>

<p align="center">
  <a href="">
    <img alt="Bilbo boilerplate" src="src/assets/img/logo.svg" width="600" />
  </a>
</p>

<p align="center">
  <em>
  SASS
  · Babel
  · Bootstrap
  · Gulp
  · JQuery
  · PopperJS
  · Browsersync
  </em>
</p>

This Gulp-Sass boilerplate starter contains the features and scripts you need to get started quickly with Gulp Runner and building, Live Loading.

It contains the following features:

- GulpJS
- Babel ES6 Compiler
- Bootstrap v4
- JQuery v3.3.1
- PopperJS
- Concatenate and minify JavaScript.
- Compile, minify, autoprefix SASS.
- Browser-Sync Hot-Reloading
- Optimize and Cache Images

## Features

### Gulp Loaders and Plugins

This project contains the following loaders & plugins:

- `node-sass` for compiling sass (SCSS)
- `gulp-babel` for compiling ES6 code
- `Browser-sync` for hot-reloading
- `gulp-uglify` for compressing JS
- `gulp-clean-css` for compressing CSS
- `gulp-sourcemaps` for mapping into css file
- `gulp-rev` for filename hashing
- `gulp-imagemin` for optimising images

<hr />

## Getting Started

### Dependencies

Make sure these are installed first.

- [Node.js](http://nodejs.org)
- [Gulp Command Line Utility](http://gulpjs.com)

     `npm install --global gulp-cli`

### Quick Start

1. Clone the repo :
    
      `git clone https://github.com/Createstorm/bilbo-boilerplate.git`
      
2. In bash/terminal/command line, `cd bilbo-boilerplate` into project directory.
3. Run `npm install` to install required files and dependencies.
4. Launch the `development environment` with :

    `
    gulp shire
    `

    then, navigate to http://localhost:3000 (it will open automatically)

Note: **For Production, Use:**

```
gulp adventure
```
This will build files and assets to `dist` directory.

<hr />

## Documentation

### Workflow structure

`src` - > source directory

`dist` -> build directory


```
.
├── src
│   ├── assets
│   │   └── logo.svg
│   ├── sass
│   │   ├── _fonts.scss
│   │   ├── _variables.scss
│   │   └── main.scss
│   ├── index.js
│   └── index.html
.

.
├── dist
│   ├── assets
│   │   ├── logo.svg
│   ├── css
│   │   └── style.min.css
│   ├── js
│   │   └── bundle.min.js
│   └── index.html
.
```

### Instructions

- Add your HTML files by inserting or including them in the `src` directory (By default `index.html` is added to the directory, feel free to edit it with the changes seen live.)
  - For the new `HTML` file(s), link the `styles.css` (in head tag) and `bundle.js` (in body tag) file in the `HTML` files as they are created.
      ```
      <head>
          :
          <link rel="stylesheet" href="css/style.css" />
      </head>
      <body>
          : 
          <script src="js/bundle.js"></script>
      </body>
      ```

- Add `sass`(SCSS) files to `src/sass` folder.

    - Make sure you import the scss file in `main.scss`
      ```
      @import "filename";
      ```

- Add `images` to `src/assets` folder.

## TODO list

- [x] Bootstrap inclusion
- [x] Jquery
- [x] PopperJS
- [ ] CSS Loaders
- [x] Assets Loader
- [x] Separated location for Bundled Files
- [ ] Zip Plugin
- [ ] Code Optimising

## Licence

Code released under the [MIT License](https://github.com/abhijithvijayan/gulp-sass-boilerplate/blob/master/LICENCE).
