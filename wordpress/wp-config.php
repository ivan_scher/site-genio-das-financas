<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'geniofinancas' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';N/N|V_({oki^V%R9YKMcY^t(AB?.%!a~rck#PJ-p+}p=SFJS7%e:>dZQ25e7QUs' );
define( 'SECURE_AUTH_KEY',  '>$/`{gfLf;[[9UiRDYzXu=a+NE,?G2NVcezSv /hs9=:j:dtvi<MO3`XM0-C&`_^' );
define( 'LOGGED_IN_KEY',    '~w:lP[A;cQO={S}DY<(-Svwr-*/_pobt#wXok8; Eh@<c hoSDE[8h9;GD:Og?6I' );
define( 'NONCE_KEY',        'KL,o2v@;.FSuMf@@6 jpC:M<n,:w;A]A$b]WHm-6K;N#gs=o*=O(TU!pcX>3&T6i' );
define( 'AUTH_SALT',        'L <O^*dLV#.>hb=5lfyUnd.IHBxQuI?Yhb$-/DaoMAu`G?C3;IOYmj_V{83}GfN>' );
define( 'SECURE_AUTH_SALT', 'U_Poib.14*t-:6c&>{-2ZY5auZ5g+^&I e|WP<^O$`+E99{EQZ}A!Lffak#B_?MN' );
define( 'LOGGED_IN_SALT',   'L!u%(gCK9I}9{u)#EU_26FOS0%%8SEs^T0}#W2gxZXHvY;9l{?d>0{;-!.jK(vCl' );
define( 'NONCE_SALT',       'gSNFYI%9~8,iIFQHH[Hy*_UO:V~+Rn*Xp,$RkfP:TA}4,9!U.3oI%E30oa;,b,!~' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'gfei_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
