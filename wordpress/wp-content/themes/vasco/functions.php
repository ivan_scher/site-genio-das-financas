<?php

// Registrando Imagem Destaque
if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );

if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'quadrada', 1170, 1170, true );
    add_image_size( 'horizontal', 1170, 640, true );
    add_image_size( 'vertical', 640, 1170, true );
    add_image_size( 'logo', 320, 320, true );
    add_image_size( 'banner', 1920, 680, true );
}

add_filter( 'wp_image_editors', 'change_graphic_lib' );

function change_graphic_lib($array) {
    return array( ‘WP_Image_Editor_GD’, ‘WP_Image_Editor_Imagick’ );
}

/*
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}*/


/*INSERINDO A IMAGEM DE DESTAQUE PARA O FEED*/
add_filter('the_content_feed', 'rss_post_thumbnail');
function rss_post_thumbnail($content) {
  global $post;
  if( has_post_thumbnail($post->ID) )
    $content = '<p>' . get_the_post_thumbnail($post->ID, 'thumbnail') . '</p>' . $content;
  return $content;
}


/* Adicionando Menu*/
add_theme_support( 'menus' );
if ( function_exists( 'register_nav_menu' ) ) {
    register_nav_menu( 'menu', 'Menu Principal' );
}

/* Alterando o Texto Post para Nóticias
add_filter(  'gettext',  'change_post_to_article'  );

function change_post_to_article( $translated ) {
    $translated = str_ireplace(  'Posts',  'Cardápio',  $translated );
    return $translated;
}*/

/* Desabilitando o Auto Salvamento (bug campos personalizados)*/
function disable_autosave() {
    wp_deregister_script('autosave');
}
add_action( 'wp_print_scripts', 'disable_autosave' ); 

/* Removendo status de atualização do usuário*/
global $user_login;
get_currentuserinfo();
if (!current_user_can('update_plugins')) { 
	add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
	add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );
}

/* Texto do rodapé da administração*/
function fn_admin_footer_text() {
	echo 'Site Desenvolvido por: <a href="http://www.createstorm.com.br/" target="_blank">Create Storm</a>.';
}
add_filter( 'admin_footer_text', 'fn_admin_footer_text');

/* Adicionando Menu*/
add_theme_support( 'menus' );
if ( function_exists( 'register_nav_menu' ) ) {
    register_nav_menu( 'menu', 'Menu Principal' );
}


/*Excluindo dashboard*/
function del_secoes_painel(){
  global$wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_options']);
}

add_action('wp_dashboard_setup', 'del_secoes_painel');

function remove_menus(){
  remove_menu_page( 'upload.php' );                 /*Media*/
  remove_menu_page( 'edit-comments.php' );          /*Comments*/
  remove_menu_page( 'themes.php' );                 /*Appearance*/
}
add_action( 'admin_menu', 'remove_menus' );

include('custom/funcoes.php');

/* Ordenar os itens do Menu!*/
function custom_menu_order($menu_ord) {
       if (!$menu_ord) return true;
       return array(
        'index.php',
        'edit.php'

    );
   }
add_filter('custom_menu_order', 'custom_menu_order');
add_filter('menu_order', 'custom_menu_order');



?>