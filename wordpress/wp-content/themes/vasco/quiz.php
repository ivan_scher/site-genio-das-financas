<?php 
//Template Name: Quiz
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php wp_title('&lsaquo;', true, 'right'); ?> </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- inject css -->
        <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/style-1e5e50921f.css" />
        
        <?php wp_head();?>
        <style>
        .navbar.quiz{
            background-color: #e63440;
        }
        
       
input[type="button"]{
    font-family: 'Bebas Neue', cursive;
    padding: 0 20px;
    line-height: 36px;
    border: none;
    outline: none;
    color: #6c28e0;
    background-color: #01c8a7;
    border-radius: 50px;
    text-transform: uppercase;
}



form div{position: relative; width:calc(100% - 40px); margin: 0 0 0 20px ; display: inline-block;}

.quiz-form input[type="radio"],
.quiz-form input[type="checkbox"] {
    opacity: 0;
}

.quiz-form label {
    position: relative;
    display: inline-block;
    
    /*16px width of fake checkbox + 6px distance between fake checkbox and text*/
    padding-left: 22px;
}

.quiz-form label::before,
.quiz-form label::after {
    position: absolute;
    content: "";
    
    /*Needed for the line-height to take effect*/
    display: inline-block;
}

/*Outer box of the fake checkbox*/
.quiz-form label::before{
    height: 16px;
    width: 16px;
    
    border: 1px solid #9C27B0;
    left: 0px;
    
    background: #9C27B0;
    /*(24px line-height - 16px height of fake checkbox) / 2 - 1px for the border
     *to vertically center it.
     */
    top: 3px;
}

/*Checkmark of the fake checkbox*/
.quiz-form label::after {
    height: 5px;
    width: 9px;
    border-left: 2px solid;
    border-bottom: 2px solid;
    border-color:white;
    transform: rotate(-45deg);
    
    left: 4px;
    top: 7px;
}

/*Hide the checkmark by default*/
.quiz-form input[type="checkbox"] + label::after {
    content: none;
}

/*Unhide on the checked state*/
.quiz-form input[type="checkbox"]:checked + label::after {
    content: "";
}

/*Adding focus styles on the outer-box of the fake checkbox*/
.quiz-form input[type="checkbox"]:focus + label::before {
    outline: ##01c8a7 auto 5px;
}
form div{
    margin-left: 0 !important;
}
@media (max-width: 780px){
    .py-5 {
        padding-top: 3rem!important; 
        margin-top: 0;
    }
}

        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg fixed-top navbar-fixed-top quiz">
            <div class="container">
                <a class="navbar-brand" href="#home"><img src="<?php bloginfo('template_url') ?>/assets/logo.png" alt="genio das financas" style="max-height: 70px;"></a>
            
                
                
                  <div class="navbar-nav">
                    <a class="nav-item nav-link mx-2" href="<?php bloginfo('url'); ?>/#home">HOME </a>
                    </div>
              
            </div>
        </nav>
        
         <section id="quiz" data-spy="scroll" data-offset="50" style="background-image:none; min-height: auto;">
             <div class="container py-5">
                 <div class="row pt-5">

                 <div class="col-12 col-lg-6  text-white" >
                    <img src="<?php bloginfo('template_url') ?>/assets/text-quiz.png" class="img-fluid mb-5 "  alt="">
                    <?php the_content();?> 
                </div>
                <div class="col-12 col-lg-6">
                    <img src="<?php bloginfo('template_url') ?>/assets/img-quiz.png" class="img-fluid mt-5 " alt="">
                </div>
                </div>
             </div>
         </section>
         <section id="quiz" class="text-white" data-spy="scroll" data-offset="50">
             <div class="container py-5">
                
                 <div id="confucio" class="row py-5 mb-5 " >
                     <div class="col-12 col-lg-8 offset-lg-2 py-5">
                         <p class="text-center">Se seu plano é para um ano, plante arroz.
                            Se seu plano é para daqui dez anos, plante árvores.
                            Mas se seu plano for para
                             durar cem anos, eduque as crianças.”</p>
                             <h3 class="text-center">Confúcio</h3>
                     </div>
                 </div>
                 <div class="row">
                     
                    <div class="col-12 text-center">
                        <a href="" >
                            <img src="<?php bloginfo('template_url') ?>/assets/assinatura.png" class="img-fluid" alt="">
                        </a>
                    </div>
                 </div>
             </div>
         </section>
        
        <!-- inject js -->
        <script src="<?php bloginfo('template_url') ?>/js/bundle-0f59c378be.js"></script>
        <?php wp_footer();?>
    </body>
</html>