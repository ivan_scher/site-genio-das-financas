<?php 
//Template Name: Resultado
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php wp_title('&lsaquo;', true, 'right'); ?> </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- inject css -->
        <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/style-1e5e50921f.css" />
        <style>
        .navbar.quiz{
            background-color: #e63440;
        }
        
       
input[type="button"]{
    font-family: 'Bebas Neue', cursive;
    padding: 0 20px;
    line-height: 36px;
    border: none;
    outline: none;
    color: #6c28e0;
    background-color: #01c8a7;
    border-radius: 50px;
    text-transform: uppercase;
}
        </style>
        <?php wp_head();?>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg fixed-top navbar-fixed-top quiz">
            <div class="container">
            <a class="navbar-brand" href="#home"><img src="<?php bloginfo('template_url') ?>/assets/logo.png" alt="genio das financas" style="max-height: 70px;"></a>
           
            
                 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <img src="<?php bloginfo('template_url') ?>/assets/iconfinder_menu-alt_134216.svg" alt="" width="30">
            </button>   
            <div class="collapse navbar-collapse justify-content-end " id="navbarNavAltMarkup" hidden>
                <div class="navbar-nav">
                <a class="nav-item nav-link mx-2" href="<?php bloginfo('url'); ?>/#home">VOLTAR PARA A HOME </a>
                </div>
            </div>
            </div>
        </nav>
        
         <section id="quiz" data-spy="scroll" data-offset="50" style="background-image:none; min-height: auto;">
             <div class="container py-5">
                 <div class="row pt-5">

                 <div class="col-12 text-white" >
                    <img src="<?php bloginfo('template_url') ?>/assets/text-quiz.png" class="img-fluid mb-5 "  alt="">
                    <?php the_content();?> 
                </div>
                </div>
             </div>
         </section>
         <section id="quiz" class="text-white" data-spy="scroll" data-offset="50">
             <div class="container py-5">
                
                 <div id="confucio" class="row py-5 mb-5 " >
                     <div class="col-12 col-lg-8 offset-lg-2 py-5">
                         <p class="text-center">Se seu plano é para um ano, plante arroz.
                            Se seu plano é para daqui dez anos, plante árvores.
                            Mas se seu plano for para
                             durar cem anos, eduque as crianças.”</p>
                             <h3 class="text-center">Confúcio</h3>
                     </div>
                 </div>
                 <div class="row">
                     
                    <div class="col-12 text-center">
                        <a href="" >
                            <img src="<?php bloginfo('template_url') ?>/assets/assinatura.png" class="img-fluid" alt="">
                        </a>
                    </div>
                 </div>
             </div>
         </section>
        
        <!-- inject js -->
        <script src="<?php bloginfo('template_url') ?>/js/bundle-0f59c378be.js"></script>
        <?php wp_footer();?>
    </body>
</html>