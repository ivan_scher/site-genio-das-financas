<?php
/**
 * Adicionamos uma acção no inicio do carregamento do WordPress
 * através da função add_action( 'init' )
 */
add_action( 'init', 'create_post_type_portfolio' );
/**
 * Esta é a função que é chamada pelo add_action()
 */
function create_post_type_portfolio() {
    /**
     * Labels customizados para o Unidade de post
     *
     */
    $labels = array(
        'name' => _x('portfolio', 'post type general name'),
        'singular_name' => _x('portfolio', 'post type singular name'),
        'add_new' => _x('Adicionar nova', 'portfolio'),
        'add_new_item' => __('Adicionar nova portfolio'),
        'edit_item' => __('Editar portfolio'),
        'new_item' => __('Nova portfolio'),
        'all_items' => __('Todos as portfolios'),
        'view_item' => __('Ver portfolio'),
        'search_items' => __('Encontrar trabalho'),
        'not_found' =>  __('Nenhuma portfolio Encontrado'),
        'not_found_in_trash' => __('Nenhuma portfolio Encontrado na Lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Portfolio'
    );

    /**
     * Registamos o Unidade de post film através desta função
     * passando-lhe os labels e parâmetros de controlo.
     */
    register_post_type( 'portfolio', array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'has_archive' => 'portfolio',
            'rewrite' => array(
                'slug' => 'portfolio',
                'with_front' => false,
            ),
            'capability_type' => 'post',
            'has_archive' => false,
            'hierarchical' => false,
            'menu_position' => null,
            'rest_base'          => 'portfolio-api',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
            'supports' => array('title', 'editor', 'thumbnail', 'excerpt')
        )
    );


    /**
     * Registamos a categoria de filmes para o Unidade de post film

    register_taxonomy( 'portfolio_category', array( 'portfolio' ),
        array(
            'hierarchical' => true,
            'has_archive' => false,
            'label' => __( 'Categorias' ),
            'labels' => array( // Labels customizadas
                'name' => _x( 'Categorias', 'taxonomy general name' ),
                'singular_name' => _x( 'Categorias', 'taxonomy singular name' ),
                'search_items' =>  __( 'Search Categorias' ),
                'all_items' => __( 'Todos 0s Categorias' ),
                'parent_item' => __( 'Parent Categoria' ),
                'parent_item_colon' => __( 'Parent Categoria:' ),
                'edit_item' => __( 'Editar Categoria' ),
                'update_item' => __( 'Update Categoria' ),
                'add_new_item' => __( 'Add Novo Categoria' ),
                'new_item_name' => __( 'Novo Categoria' ),
                'Unidade_name' => __( 'Categoria' ),
            ),
            'show_ui' => true,
            'show_in_tag_cloud' => true,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'portfolio-categoria',
                'with_front' => false
            ),
            'show_in_rest'       => true,
            'rest_base'          => 'portfoliocategoria',
            'rest_controller_class' => 'WP_REST_Terms_Controller',
        )
    );
     * */
    /**
     * Esta função associa Unidades de categorias com Unidades de posts.
     * Aqui associamos as tags ao Unidade de post film.
     */
    register_taxonomy_for_object_type( 'portfolio-categorias','adc_pfinder', 'portfolio' );
    //$taxonomies   = array( 'portfolio-categoria' );
    flush_rewrite_rules();


}