<?php
/**
 * Adicionamos uma acção no inicio do carregamento do WordPress
 * através da função add_action( 'init' )
 */
add_action( 'init', 'create_post_type_cliente' );
/**
 * Esta é a função que é chamada pelo add_action()
 */
function create_post_type_cliente() {    
    /**
     * Labels customizados para o tipo de post
     * 
     */
    $labels = array(
	    'name' => _x('cliente', 'post type general name'),
	    'singular_name' => _x('cliente', 'post type singular name'),
	    'add_new' => _x('Adicionar novo', 'cliente'),
	    'add_new_item' => __('Adicionar novo cliente'),
	    'edit_item' => __('Editar cliente'),
	    'new_item' => __('Novo cliente'),
	    'all_items' => __('Todos os clientes'),
	    'view_item' => __('Ver artigo'),
	    'search_items' => __('Encontrar cliente'),
	    'not_found' =>  __('Nenhum cliente Encontrado'),
	    'not_found_in_trash' => __('Nenhum cliente Encontrado na Lixeira'),
	    'parent_item_colon' => '',
	    'menu_name' => 'Clientes'
    );
    
    /**
     * Registamos o tipo de post film através desta função
     * passando-lhe os labels e parâmetros de controlo.
     */
    register_post_type( 'cliente', array(
	    'labels' => $labels,
	    'public' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true,
	    'show_in_menu' => true,
	    'has_archive' => 'cliente',
	    'rewrite' => array(
		 'slug' => 'cliente',
		 'with_front' => false,
	    ),
	    'capability_type' => 'post',
	    'has_archive' => false,
	    'hierarchical' => false,
	    'menu_position' => null,
	    'supports' => array('title', 'thumbnail', 'excerpt')
	    )
    );
    
    /** 
     * Esta função associa tipos de categorias com tipos de posts.
     * Aqui associamos as tags ao tipo de post film.
     */
    register_taxonomy_for_object_type( 'cliente-categorias','adc_pfinder', 'cliente' );
	
	flush_rewrite_rules();
    
}
