<?php 
//Template Name: Principal
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php wp_title('&lsaquo;', true, 'right'); ?> </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- inject css -->
        <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/style-1e5e50921f.css" />
        <?php wp_head();?>
        <style>
        b, strong {
    font-weight: 700;
}
.btn:hover img{
    filter: brightness(0) invert(1);
}

#material .form{
    overflow: hidden
}
/* borda e background do campo cidade */
.select2-choice, .select2-container {
    background-color: transparent !important;
}

.select2-choice {
    border: none !important;
}

/* margem do campo cidade */
#select2-chosen-3 {
    padding-left: 10px !important;
}
#rd-form-joq3m2m5i .bricks-form__label{
    color: #555 !important;
}

/* nome dos campos dentro da caixa */
::-webkit-input-placeholder {
   color: #585857 !important;
}

:-moz-placeholder { /* Firefox 18- */
   color: #585857 !important;
}

::-moz-placeholder {  /* Firefox 19+ */
   color: #585857 !important;
}

:-ms-input-placeholder {  
   color: #585857 !important;
}

/* ocultar nomes dos campos */
form#conversion-form label {
    display: none !important;    
}
@media (max-width: 780px){
    #home,
    #home .py-5 {
    padding-top: 3rem!important; 
    margin-top: 0;
}
}
</style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg fixed-top navbar-fixed-top">
            <div class="container">
            
            <a class="navbar-brand" href="#home"><img src="<?php bloginfo('template_url') ?>/assets/logo.png" alt="genio das financas" style="max-height: 70px;"></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <img src="<?php bloginfo('template_url') ?>/assets/iconfinder_menu-alt_134216.svg" alt="" width="30">
            </button>
            <div class="collapse navbar-collapse justify-content-end " id="navbarNavAltMarkup">
                <div class="navbar-nav">
                <a class="nav-item nav-link mx-2" href="#programa">O Programa <span class="sr-only">(current)</span></a>
                
                <a class="nav-item nav-link mx-2" href="#beneficios">Benefícios</a>
                <a class="nav-item nav-link mx-2" href="#material">Material</a>
                <a class="nav-item nav-link mx-2" href="#quiz">Quiz</a>
                
                </div>
            </div>
            </div>
        </nav>
        <section id="home" data-spy="scroll" data-offset="50">
            <div class="container">
                <div class="row py-5">
                    <div class="col-12 col-lg-10 offset-lg-1">
                        <img src="<?php echo  CFS()->get( 'banner_desktop' ); ?>" alt="" class="img-fluid desktop scrollnimation" data-animation="fadeIn">
                        <img src="<?php echo  CFS()->get( 'banner_mobile' ); ?>" alt="" class="img-fluid mobile">
                        <a href="#main" data-spy="scroll" data-offset="50" >
                            <img src="<?php bloginfo('template_url') ?>/assets/arrow-down.png" alt="" class="img-fluid floating" >
                        </a>
                    </div>
                </div>
                
            </div>
        </section>
        <section id="main" data-spy="scroll" data-offset="50">
            <div class="container  py-5">
                <div class="row mb-5 pt-5">
                    <div class="col-12 col-md-10 col-lg-8 scrollnimation" data-animation="fadeIn">
                        <h1 class="text-white"><?php echo  CFS()->get( 'texto_main' ); ?></h1>
                    </div>
                </div>
                <div class="row pb-5">
                    <div class="col-12 col-md-6 col-lg-5">
                        <img src="<?php bloginfo('template_url') ?>/assets/text-genio.png" alt="" class="img-fluid scrollnimation" data-animation="slideRight">
                    </div>
                    
                    <div class="col-12 col-md-6 col-lg-5 offset-lg-1 scrollnimation" data-animation="slideUp">
                        <div id="balloon">
                            <p><?php echo  CFS()->get( 'texto_balao_1' ); ?></p>
                                <p>-------------------------------</p>
                                <a href="#contato" class="btn">Quero decolar <img src="http://geniodasfinancas.com.br/wp-content/themes/vasco/assets/down.svg" width="25" alt=""></a>
                        </div>
                        <img src="<?php bloginfo('template_url') ?>/assets/img-main.png" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
            
            
         </section>
         <section id="programa" class="py-5 text-white" data-spy="scroll" data-offset="50">
             <div class="container py-5">
                 <div class="row">
                     <div class="col-12 col-md-7 col-lg-6 col-xl-5">
                         <img src="<?php bloginfo('template_url') ?>/assets/text-programa.png" alt="" class="img-fluid mt-5 mb-3 scrollnimation" data-animation="slideRight">
                        
                     </div>
                 </div>
                 <div class="row mb-5 mt-3">
                     <div class="col-12 col-lg-6 scrollnimation" data-animation="fadeIn">
                        <h2 class="text-white"><?php echo  CFS()->get( 'titulo_programa_1' ); ?></h2>
                         <div class="borderleft mb-5"><?php echo  CFS()->get( 'texto_programa_1' ); ?>
                        </div>
                        <h2><?php echo  CFS()->get( 'titulo_programa_2' ); ?></h2>
                        <div class="borderleft"><?php echo  CFS()->get( 'texto_programa_2' ); ?></div>
                        
                     </div>
                     <div class="col-12 col-lg-6">
                         <img src="<?php bloginfo('template_url') ?>/assets/foguete.png" class="img-fluid scrollnimation" data-animation="slideUp" alt="">
                     </div>
                 </div>
                 <div class="row pt-5">
                     <div class="col-12 col-lg-7">
                         <h2><?php echo  CFS()->get( 'titulo_programa_3' ); ?></h2>
                     </div>
                 </div>
                 <div class="row pb-5">
                 <?php
                    $fields = CFS()->get( 'programa_porque' );
                    $count = 0;
                    foreach ( $fields as $field ) {

                    ?>
                     <div class="col-12 col-lg-4">
                         <img src="<?php  echo $field['image']; ?>" class="img-fluid scrollnimation" data-animation="slideExpandUp" alt="">
                         <h3><?php  echo $field['titulo']; ?></h3>
                         <div><?php  echo $field['texto']; ?></div>
                     </div>
                    <?php
                    $count ++;
                    }
                    ?>
                </div>
                <div class="row ">
                    <div class="col-12 col-md-6">
                        <img src="<?php bloginfo('template_url') ?>/assets/text-realidade.png" alt="" class="img-fluid scrollnimation" data-animation="slideRight">
                    </div>
                    
                    <div class="col-12 col-md-6 scrollnimation" data-animation="slideUp">
                        <div id="balloon">
                            <p><?php echo  CFS()->get( 'texto_balao_2' ); ?></p>
                                <p>-------------------------------</p>
                                <a href="#contato" class="btn">Quero decolar <img src="http://geniodasfinancas.com.br/wp-content/themes/vasco/assets/down.svg" width="25" alt=""></a>
                        </div>
                        <img src="<?php bloginfo('template_url') ?>/assets/img-realidade.png" alt="" class="img-fluid">
                    </div>
                </div>
             </div>
             
         </section>
         <section id="beneficios" data-spy="scroll" data-offset="50">
             <div class="container py-5">
                 <div class="row py-5">
                     <div class="col-12 col-lg-6 scrollnimation" data-animation="fadeIn">
                         <img src="<?php bloginfo('template_url') ?>/assets/text-beneficios.png" alt="" class="img-fluid mt-5 mb-3 scrollnimation" data-animation="slideRight">
                     
                     </div>

                     <div class="col-12 col-lg-6 scrollnimation" data-animation="fadeIn">
                         <img src="<?php bloginfo('template_url') ?>/assets/moedas.png" alt="" class="img-fluid mt-5 mb-3 scrollnimation" data-animation="slideUp">
                        
                     </div>

                     <div class="col-12 col-lg-4 scrollnimation" data-animation="fadeIn">
                         <h2> <?php echo  CFS()->get( 'titulo_beneficio_1' ); ?> </h2>
                     </div>
                 </div>
                 <div class="row pb-5 numbers">
                    <?php
                    $fields = CFS()->get( 'aluno_aprende' );
                    $count = 1;
                    foreach ( $fields as $field ) {

                    ?>
                     <div class="col-12 col-md-6 eachnumber">
                         <div class="row d-flex align-items-center">
                         <h3><?=$count;?></h3>
                         <p class="col"><?php  echo $field['texto']; ?></p>
                        </div>
                     </div>                    
                     <?php
                    $count ++;
                    }
                    ?>
                     <div class="col-12 col-md-6 eachnumber">
                         <img src="<?php bloginfo('template_url') ?>/assets/img-beneficios.jpg" class="img-fluid scrollnimation" data-animation="slideUp" alt="">
                     </div>
                     <div class="col-12 col-md-6 ">
                         <h2><?php echo  CFS()->get( 'titulo_beneficio_2' ); ?></h2>
                     </div>
                 </div>
                 <div class="row pb-5">
                     <div class="col-12 col-lg-6 pb-5">
                         <ul>
                         <?php
                    $fields = CFS()->get( 'lista_beneficios' );
                    $count = 1;
                    foreach ( $fields as $field ) {

                    ?>
                             <li><?php  echo $field['texto']; ?></li>
                             <?php
                    $count ++;
                    }
                    ?>
                         </ul>
                     </div>
                     <div class="col-12 col-lg-6 ">
                         <img src="<?php bloginfo('template_url') ?>/assets/homeporco.png" class="img-fluid scrollnimation" data-animation="slideUp" alt="">
                     </div>
                 </div>
             </div>
         </section>
         <section id="material" class="text-white" data-spy="scroll" data-offset="50">
             <div class="container py-4">
                 <div class="row py-4">
                     <div class="col-12 col-lg-6 scrollnimation" data-animation="fadeIn">
                         <img src="<?php bloginfo('template_url') ?>/assets/text-material.png"  class="img-fluid mt-5 mb-3 scrollnimation" data-animation="slideRight"  alt="">
                         <h2 class="mb-5">M<?php echo  CFS()->get( 'titulo_material_1' ); ?></h2>
                         <h3 class=" mb-5"><?php echo  CFS()->get( 'titulo_material_2' ); ?>
                            </h3>
                     </div>
                     <div class="col-12 col-lg-6 pt-5">

                         <img src="<?php bloginfo('template_url') ?>/assets/img-material.png" class="img-fluid mt-5 scrollnimation" data-animation="slideUp" alt="">
                     </div>
                 </div>
                 <div class="row">
                 <?php
                    $fields = CFS()->get( 'material_modulo' );
                    $count = 1;
                    foreach ( $fields as $field ) {

                    ?>
                     <div class="col-12 col-lg-3">
                         <img src="<?php  echo $field['imagem']; ?>" class="img-fluid scrollnimation" data-animation="slideExpandUp" alt="">
                         <h4><?php  echo $field['titulo']; ?></h4>
                         <p><?php  echo $field['texto']; ?></p>
                     </div>
                     <?php
                    $count ++;
                    }
                    ?>
                 </div>
                 
                 <div class="row py-5">
                     <div class="col-12 col-lg-6">
                    <h2 class="mb-5"><?php echo  CFS()->get( 'titulo_material_3' ); ?></h2>
                     
                    </div>
                 </div>
                 <div class="row ">
                     <div class="col-12">
                     <img src="<?php echo  CFS()->get( 'pack_material' ); ?>" class="img-fluid scrollnimation" data-animation="slideUp" alt="">
                    </div>
                 </div>

                 <div class="row  py-5" id="contato">
                     <div class="col-12 col-lg-6 pt-4 scrollnimation" data-animation="fadeIn">
                         <h3>Entre em contato e conheça:</h3>
                         <p class="borderleft"><?php echo  CFS()->get( 'contatos' ); ?></p>
                            
                            <img src="<?php bloginfo('template_url') ?>/assets/img-homem-material.png" class="img-fluid scrollnimation" data-animation="slideUp" alt="">
                     </div>
                     <div class="col-12 col-lg-6 pt-4">
                         <div class="form">
                         <?php
                            echo html_entity_decode(CFS()->get( 'formulario_rd'));
                            ?>
                         </div>
                     </div>
                 </div>
             </div>
         </section>
         <section id="quiz" class="text-white" data-spy="scroll" data-offset="50">
             <div class="container py-5">
                 <div class="row  py-5">
                     <div class="col-12 col-lg-6 scrollnimation" data-animation="fadeIn">
                         <img src="<?php bloginfo('template_url') ?>/assets/text-quiz.png" class="img-fluid mt-5 mb-3 scrollnimation" data-animation="slideRight" alt="">
                         <h2><?php echo  CFS()->get( 'titulo_quiz' ); ?></h2>
                         <div class="borderleft"><?php echo  CFS()->get( 'texto_quiz' ); ?></div>
                            <a href="<?php bloginfo('url'); ?>/quiz-dados/" class="btn">iniciar teste <img src="http://geniodasfinancas.com.br/wp-content/themes/vasco/assets/right.svg" width="25" alt=""></a>
                     </div>
                     <div class="col-12 col-lg-6">
                         <img src="<?php bloginfo('template_url') ?>/assets/img-quiz.png" class="img-fluid mt-5 scrollnimation" data-animation="slideUp" alt="">
                     </div>
                 </div>
                 <div id="confucio" class="row py-5 mb-5 scrollnimation" data-animation="fadeIn">
                     <div class="col-12 col-lg-8 offset-lg-2 py-5">
                         <p class="text-center">"<?php echo  CFS()->get( 'frase_sabio' ); ?>"</p>
                             <h3 class="text-center"><?php echo  CFS()->get( 'nome_sabio' ); ?></h3>
                     </div>
                 </div>
                 <div class="row">
                     
                    <div class="col-12 text-center">
                        <a href="" >
                            <img src="<?php bloginfo('template_url') ?>/assets/assinatura.png" class="img-fluid" alt="">
                        </a>
                    </div>
                 </div>
             </div>
         </section>
     
        <!-- inject js -->
        <script src="<?php bloginfo('template_url') ?>/js/bundle-0f59c378be.js"></script>
        <script>
        
            jQuery(document).ready(function($) {

                setTimeout(function(){
                document.querySelector("#rd-text_field-k7nma6ce").setAttribute('placeholder', 'Nome da escola *');    
                document.querySelector("#rd-select_field-k7nma6cf option:first-child").innerHTML='Cargo na escola *';
                document.querySelector("#rd-select_field-k7nma6cg option:first-child").innerHTML='Sua escola é *';
                document.querySelector("#rd-select_field-k7nma6ch option:first-child").innerHTML='Quantidade de alunos *';
                }, 2000);

             
            });
        </script>
    </body>
</html>